<?php 

echo $header;

//$this->load->view('include/header.php'); 
//$this->load->view('include/sidebar.php');
?>


<!-- Content Wrapper / Start -->
<div id="content-wrapper">


<!-- Titlebar
================================================== -->
<section id="titlebar">

	<!-- Container -->
	<div class="container">
	
		<div class="eight columns">
			<div class="widget-thumb">
							<a href="#"><img src="<?php echo startup_logo($startup); ?>" alt="" /></a>
						</div>
						<div class="widget-text">
							<h3><a href="#"><?php echo $startup[0]['name']; ?></a></h3>
							<h4><a href="#"><?php echo $startup[0]['shortdesc']; ?></a></h4>
						</div>
						
		</div>
		
		<div class="eight columns">
			<nav id="breadcrumbs">
				<ul>
					<li>You are here:</li>
					<li><a href="#">Home</a></li>
					<li><a href="#">Portfolio</a></li>
					<li>Moon Surface</li>
				</ul>
			</nav>
		</div>

	</div>
	<!-- Container / End -->
</section>


<!-- Content
================================================== -->

<!-- Container -->
<div class="container">

	<!-- Slider -->
	<div class="eleven alt columns">
<div class="clearfix"></div>

			<!-- Tabs Navigation -->
			<ul class="tabs-nav">
				<li class="active"><a href="#tab1">Overview</a></li>
				<li><a href="#tab2">Activity</a></li>
				<li><a href="#tab3">Comments</a></li>
			</ul>

			<!-- Tabs Content -->
			<div class="tabs-container">
				<div class="tab-content" id="tab1">
                                    <?php if((!@empty($items['image'])) or (!@empty($items['video']))) { ?>
		<!-- FlexSlider  -->
		<section class="flexslider post-img">
			
                        <div class="media">
				<ul class="slides mediaholder">
                                        <?php if(!@empty($items['video'])) { ?>
					<li>
						<a class="mfp-gallery" title="#" href="#">
							<iframe width="775" height="430" src="<?php echo $items['video']; ?>" frameborder="0" allowfullscreen></iframe>
						</a>
                                        </li><?php } ?>
                                        <?php if(!@empty($items['image'])) { ?>
					<li>
						<a class="mfp-gallery" title="#" href="#">
							<iframe width="775" height="430" src="<?php echo base_url()."uploads/startups/".$items['image']; ?>" frameborder="0" allowfullscreen></iframe>
						</a>
                                        </li><?php } ?>
                                       

				</ul>
                        </div>
		</section><?php } ?>
			
<div class="eleven alt columns">
<div class="widget" style="margin: 5px 0 0 0;">
			<h3 class="headline">Description</h3><span class="line"></span><div class="clearfix"></div>
			<p><?php echo $startup[0]['description']; ?></p>
		</div>

		<!-- Job Description -->
		<div class="widget" style="margin: 25px 0 0 0;">
			<h3 class="headline" >Project Details</h3><span class="line"></span><div class="clearfix"></div>
			
			<ul class="list-3" style="margin: 5px 0 18px 0; list-style-type:none;">
			<?php if(!empty($items['website'])) { ?>	
                            <li>Web Site : <a href="http://<?php echo @$items['website']; ?>" target="_blank">www.<?php echo @$items['website']; ?></a></li>
				<?php } ?>
                                <?php if(!empty($items['founded'])) { ?>
                                <li>Founded : <?php echo @$items['founded']; ?></li>
                                <?php } ?>
                            <?php if(!empty($items['email'])) { ?>
                                <li>Email : <?php echo @$items['email']; ?></li>
                                <?php } ?>
                                <?php if(!empty($items['location'])) { ?>
                                <li>Location: <?php echo @$items['location']; ?></li>
                                <?php } ?>
                        </ul>
<div class="clearfix"></div>
	<ul class="social-icons" style="list-style-type:none;">
                 <?php if(!empty($items['twitter'])) { ?>
               <li><a class="twitter" href="http://twitter.com/<?php echo @$items['twitter']; ?>" target="_blank"><i class="icon-twitter"></i></a></li> 
           <?php } ?>
                 <?php if(!empty($items['linkedin'])) { ?>
               <li><a class="linkedin" href="http://www.linkedin.com/company/<?php echo @$items['linkedin']; ?>" target="_blank"><i class="icon-linkedin"></i></a></li> 
           <?php } ?>
                <?php if(!empty($items['facebook'])) { ?>
               <li><a class="facebook" href="http://facebook.com/<?php echo @$items['facebook']; ?>" target="_blank"><i class="icon-facebook"></i></a></li> 
           <?php } ?>
          
			</ul>
            
            		
			<a href="#" class="button color launch">Follow</a>
			<div class="clearfix"></div>
				<?php if(!empty($incubator)) {  ?>
                        <h3 class="headline" >Incubator</h3><span class="line"></span><div class="clearfix"></div>
<ul class="widget-tabs comments">
					
                                <!-- Comment #1 -->
                                <li>
                                        <div class="widget-thumb">
                                                <a href="<?php echo base_url().'incubator/'.$incubator['seo']; ?>"><img src="<? echo inc_logo($incubator);?>" alt="" /></a>
                                        </div>

                                        <div class="widget-text">
                                                <h4><a href="<?php echo base_url().'incubator/'.$incubator['seo']; ?>"><?php echo $incubator['incname'];?> </a></h4>
                                                <span><?php echo $incubator['desc'];?></span>
                                        </div>
                                        <div class="clearfix"></div>
                                </li>

					
			</ul>
                                <div class="clearfix"></div><?php } ?>
			<!-------------->
			<div class="clearfix"></div>
				<!-- TEAM -->
        <?php  if(!empty($investors)) {  ?>
                <div class="widget" style="margin: 25px 0 0 0;">
			<h3 class="headline" >Investors</h3><span class="line"></span><div class="clearfix"></div>
			
			<ul class="tab-content" style="margin: 5px 0 18px 0;">
			<!-- Recent Comments -->
				<ul class="widget-tabs comments">
				<?php   
					foreach($investors as $found){  ?>
                                        <!-- Comment #2 -->
					<li>
						<div class="widget-thumb">
							<a href="<?php echo base_url().'investor/'.$found['seo']; ?>"><img src="<?php echo investor_logo($found); ?>" alt="" /></a>
						</div>
						
						<div class="widget-text">
                                                    <h4><a href="<?php echo base_url().'investor/'.$found['seo']; ?>"><?php echo $found['name']; ?></a></h4>
	
                                                    <span><?php echo $found['desc']; ?></span>
						</div>
						<div class="clearfix"></div>
					</li>
                                        <?php  } ?>
					
			</ul>
			<div class="clearfix"></div>
			
			
        </div><?php } ?>
		
		</div>
</div>
</div><!--tab 2 end-->
<div class="tab-content" id="tab2">
<!-- activity tab -->
<h3 class="headline" >Activity</h3><span class="line"></span><div class="clearfix"></div>
			<?php if(!empty($activity)){ ?>
    
			<ul class="tab-content" style="margin: 5px 0 18px 0;">
			<!-- Recent Comments -->
				<ul class="widget-tabs comments">
				<?php	
				foreach($activity as $act) {       ?>
                    	<!-- Comment #1 -->
					<li>
						<div class="widget-thumb">
							<a href="<?php echo user_url($act); ?>"><img src="<?php echo user_image($act); ?>" alt="" /></a>
						</div>
						
						<div class="widget-text">
							<span><a href="<?php echo user_url($act); ?>"><?php echo $act['name']; ?></a></span>
							<h4><a href="#"><?php echo $act['activity']; ?></a></h4>
						</div>
						<div class="clearfix"></div>
					</li>
                                <?php } ?>
                        </ul><?php } ?>
			<div class="clearfix"></div>
</div><!--tab2 end -->
				<div class="tab-content" id="tab3">
<!-- comments tab -->
<h3 class="headline">Comments <span class="comments-amount"></span></h3><span class="line"></span><div class="clearfix"></div>
	
	<section class="comments-sec">
 
		<ol id="commentlist" class="commentlist">
		<?php if(!empty($comments)){
    foreach($comments as $comment) {       ?>
                    <li>
				<div id="1" class="comment">
					<div class="avatar"><a href="<?php echo user_url($comment); ?>"><img src="<?php echo user_image($comment); ?>" alt="" /> </a></div>
					<div class="comment-des"><div class="arrow-comment"></div>
						<div class="comment-by"><strong><a href="<?php echo user_url($comment); ?>"><?php echo $comment['name']; ?></a></strong><span class="date"><?php echo write_date($comment['date']); ?></span></div>
						<p><?php echo $comment['comment']; ?></p>
					</div>
					<div class="clearfix"></div>
				</div>

				
			</li><div class="clearfix"></div>
                <?php } }else {?> <div class="notification error closeable">
				<p><span>Error!</span> There are no comments.</p>
				<a class="close" href="#"></a>
			</div><?php } ?>
			
		 </ol>

	</section>
	<div class="clearfix"></div>
        <!-- comment form-->
        <!-- Form -->
	     
       
               <script> 
        // wait for the DOM to be loaded 
       $(document).ready(function() { 
    // bind form using ajaxForm 
    $('#comment').ajaxForm({ 
        // dataType identifies the expected content type of the server response 
            beforeSubmit: validate,
            dataType:  'json', 
 
        // success identifies the function to invoke when the server response 
        // has been received 
        success:   processJson 
    }); 
});
function processJson(data) { 
    // 'data' is the json object returned from the server 
   
        console.log(data);
        if(data.message="success"){
            $('#commentlist').append(' <li><div id="1" class="comment"><div class="avatar"><a href="<?php echo user_url($comment); ?>"><img src="<?php echo user_image($comment); ?>" alt="" /> </a></div><div class="comment-des"><div class="arrow-comment"></div><div class="comment-by"><strong><a href="<?php echo user_url($comment); ?>"><?php echo $comment['name']; ?></a></strong><span class="date">now</span></div><p>'+data.comment+'</p></div><div class="clearfix"></div></div></li>');
            $('textarea[name=comment]').val('');

        
        }else {
    
    $('#fail').fadeIn('slow');
        };
        }
        
function validate(formData, jqForm, options) { 
 
    var form = jqForm[0]; 
    if (!form.comment.value) { 
      
        return false; 
    } 
    
}
    
    </script> 
   
       <div id="fail" style="display:none;" class="notification error closeable">
				<p><span>Error!</span> Please try again.</p>
				<a class="close" href="#"></a>
			</div>
    
    <div id="1" class="comment">
					<div class="avatar"><a href="<?php echo user_url($comment); ?>"><img src="<?php echo user_image($comment); ?>" alt="" /> </a></div>
					<div class="comment-des"><div class="arrow-comment"></div>
						<div class="comment-by"><strong><a href="<?php echo user_url($comment); ?>"><?php echo $comment['name']; ?></a></strong></div>
                                                    <p> <form id="comment" method="post" action="<?php echo '../process/add_startup_comment'; ?>"  >
                                                    <input type="hidden" name="startup" id="startup" value="<?php echo $startup[0]['sid'];?>">
					
					<textarea name="comment" cols="40" rows="1" id="comment" spellcheck="true"></textarea>
                                                <input type="submit" class="submit" id="submit" value="Send Comment" />    </form></p>
					</div>
					<div class="clearfix"></div>
				</div>
     	
          
        
				</div><!--tab 3 end-->
			</div>
	</div>
	<!-- Slider / End -->
	

	<!-- Sidebar -->
	<div class="five columns">
		
		

		<!-- Job Description -->
        <?php if(($founders!="") or ($startup[0]['founder']!="")) { ?>
                <div class="widget" style="margin: 25px 0 0 0;">
			<h3 class="headline" >Founders</h3><span class="line"></span><div class="clearfix"></div>
			
			<ul class="tab-content" style="margin: 5px 0 18px 0;">
			<!-- FOUNDERS -->
				<ul class="widget-tabs comments">
				<?php if($startup[0]['founder']!="") {
                                     $found=explode(',', $startup[0]['founder']);
                                     foreach ($found as $fyaz) { ?>
					<!-- Comment #1 -->
					<li>
						<div class="widget-thumb">
							<a href="#"><img src="http://www.gravatar.com/avatar/00000000000000000000000000000000?d=mm&amp;s=100" alt="" /></a>
						</div>
						
						<div class="widget-text">
							<h4><a href="#"><?php echo $fyaz;?></h4>
							
						</div>
						<div class="clearfix"></div>
					</li>
                                <?php } }else {    
					foreach($founders as $found){  ?>
                                        <!-- Comment #2 -->
						<li>
						
                                           <div class="widget-thumb">
                                                        <a href="<?php echo user_url($found); ?>"><img src="<?php  echo user_image($found); ?>" alt="" /></a>
						</div>
						
						<div class="widget-text">
                                                    <h4><a href="<?php  echo user_url($found); ?>"><?php echo $found['name']; ?></a></h4>
	
                                                    <span><?php echo summary($found); ?></span>
						</div>
						<div class="clearfix"></div>
					</li>
                                        <?php } } ?>
					
			</ul>
			<div class="clearfix"></div>
			
			
        </div><?php } ?>
	<!-- Job Description -->
		<!-- TEAM -->
        <?php  if(!empty($team)) { ?>
                <div class="widget" style="margin: 25px 0 0 0;">
			<h3 class="headline" >Team</h3><span class="line"></span><div class="clearfix"></div>
			
			<ul class="tab-content" style="margin: 5px 0 18px 0;">
			<!-- Recent Comments -->
				<ul class="widget-tabs comments">
				<?php   
					foreach($team as $found){  ?>
                                        <!-- Comment #2 -->
					<li>
						<div class="widget-thumb">
							<a href="<?php echo user_url($found); ?>"><img src="<?php echo user_image($found); ?>" alt="" /></a>
						</div>
						
						<div class="widget-text">
                                                    <h4><a href="<?php  echo user_url($found); ?>"><?php echo $found['name']; ?></a></h4>
	
                                                    <span><?php echo summary($found); ?></span>
						</div>
						<div class="clearfix"></div>
					</li>
                                        <?php  } ?>
					
			</ul>
			<div class="clearfix"></div>
			
			
        </div><?php } ?>
			
			<!-- advisor -->
        <?php  if(!empty($advisor)) { ?>
                <div class="widget" style="margin: 25px 0 0 0;">
			<h3 class="headline" >Advisors</h3><span class="line"></span><div class="clearfix"></div>
			
			<ul class="tab-content" style="margin: 5px 0 18px 0;">
			<!-- Recent Comments -->
				<ul class="widget-tabs comments">
				<?php   
					foreach($advisor as $found){  ?>
                                        <!-- Comment #2 -->
					<li>
						<div class="widget-thumb">
							<a href="<?php echo user_url($found); ?>"><img src="<?php echo user_image($found); ?>" alt="" /></a>
						</div>
						
						<div class="widget-text">
                                                    <h4><a href="<?php  echo user_url($found); ?>"><?php echo $found['name']; ?></a></h4>
	
                                                    <span><?php echo summary($found); ?></span>
						</div>
						<div class="clearfix"></div>
					</li>
                                        <?php  } ?>
					
			</ul>
			<div class="clearfix"></div>
			
			
        </div><?php } ?>
			
				
		</div>
	</div>
	<!-- Sidebar / End-->
	
</div>
<!-- Container / End -->

</div>
<!-- Content Wrapper / End -->


<?php echo $footer; ?>