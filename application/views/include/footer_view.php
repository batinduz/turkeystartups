


<!-- Footer
================================================== -->
<div id="footer">

	<!-- Container -->
	<div class="container">

		<div class="four columns">
			<h3>Discover</h3>
			<a href="#" class="b2b" data-show-count="false" data-dnt="true">B2B</a><br />
            <a href="#" class="blog" data-show-count="false" data-dnt="true">Blog</a><br />
            <a href="#" class="classifieds" data-show-count="false" data-dnt="true">Classifieds</a><br />
            <a href="#" class="consumer" data-show-count="false" data-dnt="true">Consumer</a><br />
            <a href="#" class="Entertainment" data-show-count="false" data-dnt="true">Entertainment</a><br />
            <a href="#" class="digital-signage" data-show-count="false" data-dnt="true">Digital Signage</a><br />
            <a href="#" class="education" data-show-count="false" data-dnt="true">Education</a>
      </div>

		<div class="four columns">
	    <h3>Menu</h3>
	<a href="#" class="b2b" data-show-count="false" data-dnt="true">Startups</a><br />
            <a href="#" class="blog" data-show-count="false" data-dnt="true">Investors</a><br />
            <a href="#" class="classifieds" data-show-count="false" data-dnt="true">Advisors</a><br />
            <a href="#" class="Entertainment" data-show-count="false" data-dnt="true">Incubators</a><br />


  </div>

		<div class="four columns">
			<h3>About</h3>
			<a href="#" class="b2b" data-show-count="false" data-dnt="true">What is Turkey Startups?</a><br />
            <a href="#" class="blog" data-show-count="false" data-dnt="true">Project Guidelines</a><br />
            <a href="#" class="classifieds" data-show-count="false" data-dnt="true">Our Team</a><br />
            <a href="#" class="consumer" data-show-count="false" data-dnt="true">Jobs</a><br />
          


    </div>

		<div class="four columns">
			<h3>Social Networks</h3>
            
              <p align="left"><a href="https://twitter.com/Turkey_Startups" class="twitter-follow-button" data-show-count="false" data-dnt="true">Follow @Turkey_Startups</a></p>
		  <script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+'://platform.twitter.com/widgets.js';fjs.parentNode.insertBefore(js,fjs);}}(document, 'script', 'twitter-wjs');</script>
            
             
             <div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/tr_TR/all.js#xfbml=1";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>
<div class="fb-like" data-href="http://facebook.com/turkeystartups" data-send="false" data-width="260" data-show-faces="true" data-font="verdana"></div>

<script type="text/javascript">
				jQuery(document).ready(function($){
					$.getJSON('twitterc461.html?url='+encodeURIComponent('statuses/user_timeline.json?screen_name=Vasterad&count=1'), function(tweets){
				$("#twitter").html(tz_format_twitter(tweets));
				}); });
			</script>
			<div class="clearfix"></div>

		 
            
             <script src="//platform.linkedin.com/in.js" type="text/javascript">
 lang: en_US
</script>
<script type="IN/FollowCompany" data-id="2686875" data-counter="none"></script>
            
            
		</div>

	</div>
	<!-- Container / End -->

</div>
<!-- Footer / End -->

<!-- Footer Bottom / Start -->
<div id="footer-bottom">

	<!-- Container -->
	<div class="container">

		<div class="eight columns">© Copyright 2013 by Turkey Startups. All Rights Reserved.</div>
		<div class="eight columns">
			<ul class="social-icons-footer">
				<li><a href="twitter.com/Turkey_Startups" class="tooltip top" title="Twitter"><i class="icon-twitter"></i></a></li>
				<li><a href="fb.com/turkeystartups" class="tooltip top" title="Facebook"><i class="icon-facebook"></i></a></li>
				<li><a href="http://www.linkedin.com/company/turkeystartups" class="tooltip top" title="LinkedIn"><i class="icon-linkedin-rect"></i></a></li>
			</ul>
		</div>

	</div>
	<!-- Container / End -->

</div>

<!-- Footer Bottom / Start -->




</body>
</html>