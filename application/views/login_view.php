<?php 

echo $header;

?>


<!-- Content Wrapper / Start -->
<div id="content-wrapper">


<!-- Titlebar
================================================== -->
<section id="titlebar">

	<!-- Container -->
	<div class="container">
	
		<div class="eight columns">
			
			<div class="widget-text">
				<h2>Login</h2>
				
				</div>
                                            
		</div>
		
		<div class="eight columns">
			<nav id="breadcrumbs">
				<ul>
					<li>You are here:</li>
					<li><a href="#">Home</a></li>
					<li><a href="#">Portfolio</a></li>
					<li>Moon Surface</li>
				</ul>
			</nav>
		</div>

	</div>
	<!-- Container / End -->
</section>


<!-- Content
================================================== -->

<!-- Container -->
<div class="container">

<div class="eight columns">

	<h3 class="headline">Login with your information</h3><span class="line" style="margin-bottom: 35px;"></span><div class="clearfix"></div>
	
	<!-- Contact Form -->
	<section id="contact" >

		<script> 
        // wait for the DOM to be loaded 
       $(document).ready(function() { 
    // bind form using ajaxForm 
    $('loginform').ajaxForm({ 
        // dataType identifies the expected content type of the server response 
            beforeSubmit: validate,
            dataType:  'json', 
 
        // success identifies the function to invoke when the server response 
        // has been received 
        success:   processJson 
    }); 
});
function processJson(data) { 
    // 'data' is the json object returned from the server 
   
        console.log(data);
        if(data.message="success"){
     // console.log(data);
          self.location="../index.php"; 
        
        }else {
    
    $('#empty').fadeIn('slow');
        };
        }
        
function validate(formData, jqForm, options) { 
 
    var form = jqForm[0]; 
    if ((!form.email.value) || (!form.password.value)) { 
      $('#empty').fadeIn('slow');
        return false; 
    } 
    
}
    
    </script> 
     
      <div id="empty" style="display:none;" class="notification error closeable">
				<p><span>Error!  </span>Email or password is incorrect.</p>
				<a class="close" href="#"></a>
			</div>

		<!-- Form -->
		<form method="post" action="login/check" name="loginform" id="loginform">

			<fieldset>

				<div>
					<label for="name" accesskey="U">Email: <span>*</span></label>
						<input name="email" type="text" id="email"  />
				</div>

				<div>
					<label for="email" accesskey="E">Password: <span>*</span></label>
					<input name="password" type="password" id="password"  />
				</div>
				

			</fieldset>

			<input type="submit" class="submit" id="submit" value="Login" />
			<div class="clearfix"></div><a href="#">Forgot Password?</a> 
                        <a href="#">Signup</a

		</form>

	</section>
	<!-- Contact Form / End -->

</div>
    <div class="eight columns">
		<h3 class="headline">Login with your social network</h3><span class="line" style="margin-bottom:25px;"></span><div class="clearfix"></div>
		<p>Praesent ut ante id metus sollicitudin sodales. Mauris dictum, lorem quis pretium arcu fringilla dictum eu eu nisl. Donec rutrum erat non arcu gravida porttitor. Nunc et magna nisi.Aliquam at erat in purus aliquet mollis. Fusce elementum velit vel dolor iaculis egestas feugiat, nibh metus ultricies quam, nec venenati.</p>
	</div>
<!-- Container / End -->
