<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Startup extends CI_Controller {

    /**
     * Index Page for this controller.
     *
     * Maps to the following URL
     * 		http://example.com/index.php/welcome
     * 	- or -  
     * 		http://example.com/index.php/welcome/index
     * 	- or -
     * Since this controller is set as the default controller in 
     * config/routes.php, it's displayed at http://example.com/
     *
     * So any other public methods not prefixed with an underscore will
     * map to /index.php/welcome/<method_name>
     * @see http://codeigniter.com/user_guide/general/urls.html
     */
   
    public function index() {
      //  $data['baslik'] = 'deneme';
        //$this->load->model('uye_model');
        //$data['sayfalar'] = $this->home_model->sayfalar();
        show_404();
      // $this->load->view('uyeol_view'); //view sayfa adı
    }
    
     function get_startup() 
    { 
        // SEF ve ID bilgilerini almak için url helperını yüklüyoruz. 
        $this->load->helper("url"); 
          $this->load->helper("general"); 
        // Url helperında bulunan segment methodunu kullanarak gerekli alanları çekiyoruz. 
        $sef = $this->uri->segment(2,0); 
      //  echo $sef;
       $this->load->model('startup_model');
         $data['startup'] =$this->startup_model->startup_data($sef);
    $startup= $data['startup'];
    $stid=$startup[0]['sid'];
    $incid=$startup[0]['incubator'];

//--
    if($stid=="") show_404 ();
    //----
    
   
    //-------
        $item =$this->startup_model->startup_items($stid);

      foreach($item as $cek) {
          $itemname=$cek['itemname'];
          //echo $cek['value'];
          $data['items'][$itemname]=$cek['value'];
    }
    //--------
      
       $data['team'] =$this->startup_model->team($stid);
         $data['advisor'] =$this->startup_model->advisor($stid);
         $data['activity'] =$this->startup_model->activity($stid);
         $data['investors'] =$this->startup_model->investor($stid);

         $data['incubator'] =$this->startup_model->get_incubator($incid);

         
         $data['comments'] =$this->startup_model->comments($stid);
            $data['founders'] =$this->startup_model->founder($stid);;

       
//     echo "<pre>";
//      print_r($data['comments']);
//        echo "</pre>";   
     
     
         $this->load->library('template');
       $this->template->goster('startup_view', $data);
    }  
   
    
    
 
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */