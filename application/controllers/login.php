<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Login extends CI_Controller {

    /**
     * Index Page for this controller.
     *
     * Maps to the following URL
     * 		http://example.com/index.php/welcome
     * 	- or -  
     * 		http://example.com/index.php/welcome/index
     * 	- or -
     * Since this controller is set as the default controller in 
     * config/routes.php, it's displayed at http://example.com/
     *
     * So any other public methods not prefixed with an underscore will
     * map to /index.php/welcome/<method_name>
     * @see http://codeigniter.com/user_guide/general/urls.html
     */
   
    public function index() {
      //  $data['baslik'] = 'deneme';
        //$this->load->model('uye_model');
        //$data['sayfalar'] = $this->home_model->sayfalar();
       $data['baslik']="Login";
        $this->load->library('template');
       $this->template->goster('login_view',$data);
      // $this->load->view('uyeol_view'); //view sayfa adı
    }
    function check () {
        
       $kullaniciadi=$this->input->post('email',TRUE);
         $sifre=$this->input->post('password',TRUE);
        
         if(empty($kullaniciadi) or empty($sifre)) {
           echo '{ "message": "error" }';
             
         }else {
             $degerler=array($kullaniciadi, $sifre);
             $this->load->model('login_model');
           $sonuc= $this->login_model->login($kullaniciadi, $sifre);
         
             if(!empty($sonuc))
             {
               
           // $this->load->library('session');
                $this->session->set_flashdata('item', 'value');

              echo '{ "message": "success" }';
             echo $this->session->set_flashdata('item');
                 print_r($this->session->all_userdata());
             }else {
               echo '{ "message": "error" }';
                 
             }
             
         }
    }
 
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */