<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Category extends CI_Controller {

    /**
     * Index Page for this controller.
     *
     * Maps to the following URL
     * 		http://example.com/index.php/welcome
     * 	- or -  
     * 		http://example.com/index.php/welcome/index
     * 	- or -
     * Since this controller is set as the default controller in 
     * config/routes.php, it's displayed at http://example.com/
     *
     * So any other public methods not prefixed with an underscore will
     * map to /index.php/welcome/<method_name>
     * @see http://codeigniter.com/user_guide/general/urls.html
     */
   
    public function index() {
      //  $data['baslik'] = 'deneme';
        //$this->load->model('uye_model');
        //$data['sayfalar'] = $this->home_model->sayfalar();
        show_404();
      // $this->load->view('uyeol_view'); //view sayfa adı
    }
    
    public function get_category() 
    { 
        // SEF ve ID bilgilerini almak için url helperını yüklüyoruz. 
        $this->load->helper("url"); 
         
        // Url helperında bulunan segment methodunu kullanarak gerekli alanları çekiyoruz. 
        $sef = $this->uri->segment(2,0); 
        echo $sef;
         
      //  $data[sayfa]=$this->sayfa_model->sayfa($sef);
      $this->load->library('template');
       $this->template->goster('category_view', '');
    }  
    
    
 
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */