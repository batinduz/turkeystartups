<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Process extends CI_Controller {

    /**
     * Index Page for this controller.
     *
     * Maps to the following URL
     * 		http://example.com/index.php/welcome
     * 	- or -  
     * 		http://example.com/index.php/welcome/index
     * 	- or -
     * Since this controller is set as the default controller in 
     * config/routes.php, it's displayed at http://example.com/
     *
     * So any other public methods not prefixed with an underscore will
     * map to /index.php/welcome/<method_name>
     * @see http://codeigniter.com/user_guide/general/urls.html
     */
   
    public function index() {
      //  $data['baslik'] = 'deneme';
        //$this->load->model('uye_model');
        //$data['sayfalar'] = $this->home_model->sayfalar();
        show_404();
      // $this->load->view('uyeol_view'); //view sayfa adı
    }
    
    public function add_startup_comment() 
    { 
       
    $comment=$this->input->post('comment');
    $startup=$this->input->post('startup');
    
   $this->load->model('process_model');
   $is=$this->process_model->add_startup_comment($comment,$startup);
   if($is==1) {
       echo '{ "message": "success",';
      echo ' "comment": "'.$comment.'" }';
   }else 
       echo '{ "message": "fail" }';
    
    }  
    
    
 
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */