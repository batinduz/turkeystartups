<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Startup_model extends CI_Model {
 function __construct() {
        parent::__construct();
    }
    /**
     * Index Page for this controller.
     *
     * Maps to the following URL
     * 		http://example.com/index.php/welcome
     * 	- or -  
     * 		http://example.com/index.php/welcome/index
     * 	- or -
     * Since this controller is set as the default controller in 
     * config/routes.php, it's displayed at http://example.com/
     *
     * So any other public methods not prefixed with an underscore will
     * map to /index.php/welcome/<method_name>
     * @see http://codeigniter.com/user_guide/general/urls.html
     */
    public function index() {
        
    }
//

    function startup_data($seo) {
       
                  $this->db->where('seo', $seo);
             $sor= $this->db->get('startups'); 
             $transfer=$sor->result_array();
              
              return $transfer;         
    }
    function startup_items($stid){
        
         $query = $this->db->query("SELECT sv.value, si.* FROM startup_items si inner join startup_values sv where sv.itemname=si.itemname and sv.startupid='".$stid."' ");

            $row = $query->result_array();

               return $row;
        
    }
     function team($stid){
        
         $query = $this->db->query("SELECT u.*, t.* FROM teams t inner join users u where t.userid=u.uid and t.startupid='".$stid."' and  t.jobtype!='founder' and t.jobtype!='advisor'  ");

            $row = $query->result_array();

               return $row;
        
    }
    
     function founder($stid){
        
         $query = $this->db->query("SELECT u.*, t.* FROM teams t inner join users u where t.userid=u.uid and t.startupid='".$stid."' and t.jobtype='founder' ");

            $row = $query->result_array();

               return $row;
        
    }
      function advisor($stid){
        
         $query = $this->db->query("SELECT u.*, t.* FROM teams t inner join users u where t.userid=u.uid and t.startupid='".$stid."' and t.jobtype='advisor' ");

            $row = $query->result_array();

               return $row;
        
    }
    
      function comments($stid){
        
         $query = $this->db->query("SELECT u.*, c.* FROM startup_comments c inner join users u where c.userid=u.uid and c.startupid='".$stid."' and c.status='1' order by c.scid asc ");

            $row = $query->result_array();

               return $row;
        
    }
      function activity($stid){
        
         $query = $this->db->query("SELECT u.*, a.* FROM startup_activity a inner join users u where a.userid=u.uid and a.startupid='".$stid."' order by date desc ");

            $row = $query->result_array();

               return $row;
        
    }
    function get_incubator($id) {
       
                  $this->db->where('iid', $id);
             $sor= $this->db->get('incubators'); 
             $transfer=$sor->row_array();
              
              return $transfer;         
    }
       function investor($stid){
        
         $query = $this->db->query("SELECT si.*, i.* FROM startup_investor si inner join investors i where si.investor=i.inid and si.startup='".$stid."' and status='1'");

            $row = $query->result_array();

               return $row;
        
    }
    
    
    

}



/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */