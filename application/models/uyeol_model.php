<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Uyeol_model extends CI_Model {
 function __construct() {
        parent::__construct();
    }
    /**
     * Index Page for this controller.
     *
     * Maps to the following URL
     * 		http://example.com/index.php/welcome
     * 	- or -  
     * 		http://example.com/index.php/welcome/index
     * 	- or -
     * Since this controller is set as the default controller in 
     * config/routes.php, it's displayed at http://example.com/
     *
     * So any other public methods not prefixed with an underscore will
     * map to /index.php/welcome/<method_name>
     * @see http://codeigniter.com/user_guide/general/urls.html
     */
    public function index() {
        
    }

    public function sayfalar() {
        //$data = $this->db->get('deneme');
        $data = $this->db->query("select * from sayfalar");
        return $data->result();
        //echo $this->db->last_query();
    }
    function kayit($degerler) {
       
        $adsoyad=$this->db->escape_str($degerler[0]);
          $kullaniciadi=$this->db->escape_str($degerler[1]);
            $sifre=md5($this->db->escape_str($degerler[2]));
              $email=$this->db->escape_str($degerler[3]);
        
              $data=array(
                  'adsoyad' => $adsoyad, 
                  'kullaniciadi'=> $kullaniciadi,
                  'sifre'=> $sifre,
                  'email' => $email);
                  
              $ekle=$this->db->insert('uyeler',$data);
              
              if($ekle) return 1;
        else return 0;
        
    }

}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */