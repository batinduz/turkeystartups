<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class General_model extends CI_Model {
 function __construct() {
        parent::__construct();
    }
    /**
     * Index Page for this controller.
     *
     * Maps to the following URL
     * 		http://example.com/index.php/welcome
     * 	- or -  
     * 		http://example.com/index.php/welcome/index
     * 	- or -
     * Since this controller is set as the default controller in 
     * config/routes.php, it's displayed at http://example.com/
     *
     * So any other public methods not prefixed with an underscore will
     * map to /index.php/welcome/<method_name>
     * @see http://codeigniter.com/user_guide/general/urls.html
     */
    public function index() {
        
    }
//

    function user_data($uid) {
       
                  $this->db->where('uid', $uid);
             $sor= $this->db->get('users'); 
             $transfer=$sor->result_array();
              
              return $transfer;         
    }
    function startup_items($stid){
        
         $query = $this->db->query("SELECT sv.value, si.* FROM startup_items si inner join startup_values sv where sv.itemname=si.itemname and sv.startupid='".$stid."' ");

            $row = $query->result_array();

               return $row;
        
    }
     function team($stid){
        
         $query = $this->db->query("SELECT u.*, t.* FROM teams t inner join users u where t.userid=u.uid and t.startupid='".$stid."' ");

            $row = $query->result_array();

               return $row;
        
    }
     function founder($stid){
        
         $query = $this->db->query("SELECT u.*, t.* FROM teams t inner join users u where t.userid=u.uid and t.startupid='".$stid."' and t.jobtype='founder' ");

            $row = $query->result_array();

               return $row;
        
    }
      function comments($stid){
        
         $query = $this->db->query("SELECT u.*, c.* FROM startup_comments c inner join users u where c.userid=u.uid and c.startupid='".$stid."' and c.status='1' ");

            $row = $query->result_array();

               return $row;
        
    }
    function item_check($title,$item) {
        if(empty($item)) return;
        else return $title.$item;
        
    }
    

}



/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */