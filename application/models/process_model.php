<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Process_model extends CI_Model {
 function __construct() {
        parent::__construct();
    }
    /**
     * Index Page for this controller.
     *
     * Maps to the following URL
     * 		http://example.com/index.php/welcome
     * 	- or -  
     * 		http://example.com/index.php/welcome/index
     * 	- or -
     * Since this controller is set as the default controller in 
     * config/routes.php, it's displayed at http://example.com/
     *
     * So any other public methods not prefixed with an underscore will
     * map to /index.php/welcome/<method_name>
     * @see http://codeigniter.com/user_guide/general/urls.html
     */
    public function index() {
        
    }
//
    function add_startup_comment($comment, $startup) {
        $comment=$this->db->escape_str($comment);
        $data = array(
   'userid' => '1',
   'startupid' => $startup ,
   'comment' => $comment ,
   'status' => '1',
   'date' => date('y-m-d')
);

$is=$this->db->insert('startup_comments', $data); 
if($is)
    return 1;
    else
    return 0;
    }
    
    

}



/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */